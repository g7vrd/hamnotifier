package uk.co.g7vrd.hamnotifier;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HamnotifierApplicationTests {

  @BeforeAll
  public static void beforeAll() {
    System.setProperty("java.awt.headless", "false");
  }

  @Test
  void contextLoads() {
  }

}
