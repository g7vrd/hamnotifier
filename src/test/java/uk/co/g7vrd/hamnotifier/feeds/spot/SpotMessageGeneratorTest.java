package uk.co.g7vrd.hamnotifier.feeds.spot;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class SpotMessageGeneratorTest {

  @Test
  public void Message_is_generated_OK() throws IOException {
    JsonNode spot = new ObjectMapper().readTree(getClass().getResourceAsStream("/jsonevents/spot.json"));

    final String message = new SpotMessageGenerator().getMessage(spot);

    assertThat(message).isEqualTo("YV1SW@null (1237km) on 18130.0 17m\n" +
      "CQ CQ TNX Keith for QSO");
  }

}
