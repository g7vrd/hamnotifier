package uk.co.g7vrd.hamnotifier.feeds.skim;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class SkimMessageGeneratorTest {

  @Test
  public void Message_is_generated_OK() throws IOException {
    JsonNode skim = new ObjectMapper().readTree(getClass().getResourceAsStream("/jsonevents/skim.json"));

    final String message = new SkimMessageGenerator().getMessage(skim);

    assertThat(message).isEqualTo("20db EA8BFK@IL38BO (5455km) on CW 14020.3 20m");
  }

}
