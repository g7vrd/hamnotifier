package uk.co.g7vrd.hamnotifier.feeds.psk;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class PskMessageGeneratorTest {

  @Test
  public void Message_is_generated_OK() throws IOException {
    JsonNode psk = new ObjectMapper().readTree(getClass().getResourceAsStream("/jsonevents/psk.json"));

    final String message = new PskMessageGenerator().getMessage(psk);

    assertThat(message).isEqualTo("-17 S57CNT@JN76GA (1504km) on 7076.3 40m");
  }

}
