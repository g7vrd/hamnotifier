package uk.co.g7vrd.hamnotifier.feeds.wspr;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class WsprMessageGeneratorTest {

  @Test
  public void Message_is_generated_OK() throws IOException {
    JsonNode wspr = new ObjectMapper().readTree(getClass().getResourceAsStream("/jsonevents/wspr.json"));

    final String message = new WsprMessageGenerator().getMessage(wspr);

    assertThat(message).isEqualTo("-11 ZL2RKL@RE68XL (2447km) on 14097.112 20m");
  }

}
