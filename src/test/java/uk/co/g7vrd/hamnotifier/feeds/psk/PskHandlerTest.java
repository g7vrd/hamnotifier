package uk.co.g7vrd.hamnotifier.feeds.psk;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.messaging.simp.stomp.StompHeaders;
import uk.co.g7vrd.hamnotifier.Notifier;

import java.util.Collections;

import static org.mockito.Mockito.verify;

public class PskHandlerTest {

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Mock
  private Notifier notifier;

  @BeforeEach
  public void initMocks() {
    MockitoAnnotations.initMocks(this);
  }

  public void aljlakdj() throws JsonProcessingException {
    PskHandler handler = new PskHandler("CALL51GN", Collections.singleton(notifier), null);

    StompHeaders headers = null;
    Object payload = objectMapper.readTree("{ \"tx\": {\"callsign\": \"CALL51GN\"} }");
    handler.handleFrame(headers, payload);

    verify(notifier).notify("PSK", "{\"tx\":{\"callsign\":\"CALL51GN\"}}");
  }

}
