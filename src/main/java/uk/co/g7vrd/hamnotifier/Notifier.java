package uk.co.g7vrd.hamnotifier;

public interface Notifier {

  void notify(String title, String body);
}
