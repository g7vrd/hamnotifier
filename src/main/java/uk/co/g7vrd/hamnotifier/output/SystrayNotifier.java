package uk.co.g7vrd.hamnotifier.output;

import org.springframework.stereotype.Component;
import uk.co.g7vrd.hamnotifier.Notifier;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

@Component
public class SystrayNotifier implements Notifier {

  private final SystemTray tray;
  private final TrayIcon trayIcon;

  public SystrayNotifier() throws AWTException {
    final PopupMenu popup = new PopupMenu();
    trayIcon = new TrayIcon(createImage("/bulb.gif", "tray icon"));
    tray = SystemTray.getSystemTray();

    tray.add(trayIcon);
  }

  private static Image createImage(String path,
                                   String description) {
    URL imageURL = SystrayNotifier.class.getResource(path);

    if (imageURL == null) {
      System.err.println("Resource not found: " + path);
      return null;
    }

    return (new ImageIcon(imageURL, description)).getImage();
  }

  @Override
  public void notify(String title, String body) {
    trayIcon.displayMessage(title, body, TrayIcon.MessageType.INFO);
  }
}
