package uk.co.g7vrd.hamnotifier.output;

import org.springframework.stereotype.Component;
import uk.co.g7vrd.hamnotifier.Notifier;

@Component
public class SysoutNotifier implements Notifier {

  @Override
  public void notify(String title,
                     String body) {
    System.out.println(title + ": " + body);
  }
}
