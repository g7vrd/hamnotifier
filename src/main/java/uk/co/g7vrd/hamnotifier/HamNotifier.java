package uk.co.g7vrd.hamnotifier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.RestTemplateXhrTransport;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;

import java.util.Collections;
import java.util.List;

@SpringBootApplication
public class HamNotifier {

  private static final int INBOUND_MESSAGE_SIZE_LIMIT = 16 * 1024 * 1024;

  public static void main(String... args) {
    System.setProperty("java.awt.headless", "false");

    final ConfigurableApplicationContext context = SpringApplication.run(HamNotifier.class, args);
    final String stompUrl = context.getEnvironment().getProperty("stomp.url");
    final String callsign = context.getEnvironment().getProperty("callsign");
    System.out.println("--stomp.url=" + stompUrl);
    System.out.println("--callsign=" + callsign);

    List<Transport> transports = Collections.singletonList(new RestTemplateXhrTransport());

    SockJsClient sockJsClient = new SockJsClient(transports);

    WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
    stompClient.setMessageConverter(new MappingJackson2MessageConverter());
    stompClient.setInboundMessageSizeLimit(INBOUND_MESSAGE_SIZE_LIMIT);


    stompClient.connect(stompUrl,
      context.getBean(NotifierStompSessionHandler.class));
  }

}
