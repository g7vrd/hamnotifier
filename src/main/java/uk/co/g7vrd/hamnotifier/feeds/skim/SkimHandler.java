package uk.co.g7vrd.hamnotifier.feeds.skim;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import uk.co.g7vrd.hamnotifier.Notifier;
import uk.co.g7vrd.hamnotifier.feeds.MessageGenerator;

import java.lang.reflect.Type;
import java.util.Collection;

@Component
public class SkimHandler implements StompFrameHandler {

  private final Collection<Notifier> notifiers;
  private final String callsign;
  private final MessageGenerator messageGenerator;

  @Autowired
  public SkimHandler(@Value("${callsign}") String callsign,
                     Collection<Notifier> notifiers,
                     SkimMessageGenerator skimMessageGenerator) {
    this.messageGenerator = skimMessageGenerator;
    Assert.notNull(callsign, "Callsign cannot be null. Specify with --callsign=XXX on the command line");
    Assert.isTrue(!callsign.isEmpty(), "Callsign must be specified. Specify with --callsign=XXX on the command line");
    this.notifiers = notifiers;
    this.callsign = callsign;
    System.out.println("Skim handler started");
  }

  @Override
  public Type getPayloadType(StompHeaders headers) {
    return JsonNode.class;
  }

  @Override
  public void handleFrame(StompHeaders headers,
                          Object payload) {
    JsonNode skim = (JsonNode) payload;

    if (skim.at("/call/callsign").asText().equals(callsign)) {
      String message = messageGenerator.getMessage(skim);
      notifiers.forEach(notifier -> notifier.notify("Skim", message));
    }
  }
}
