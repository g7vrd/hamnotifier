package uk.co.g7vrd.hamnotifier.feeds.spot;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Component;
import uk.co.g7vrd.hamnotifier.feeds.MessageGenerator;

@Component
public class SpotMessageGenerator implements MessageGenerator {

  @Override
  public String getMessage(JsonNode jsonNode) {
    final String rxCallsign = jsonNode.at("/source/callsign").asText();
    final String rxGrid = jsonNode.at("/source/grid").asText();
    final String mode = jsonNode.at("/mode").asText();
    final String band = jsonNode.at("/band").asText();
    final String khz = jsonNode.at("/khz").asText();
    final String km = jsonNode.at("/km").asText();
//    final String report = jsonNode.at("/db").asText();
    final String note = jsonNode.at("/note").asText();


    return rxCallsign + "@" + rxGrid + " (" + km + "km) on " + khz + " " + band + "\n" + note;
  }
}
