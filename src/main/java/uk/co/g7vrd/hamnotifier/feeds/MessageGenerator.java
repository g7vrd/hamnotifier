package uk.co.g7vrd.hamnotifier.feeds;

import com.fasterxml.jackson.databind.JsonNode;

public interface MessageGenerator {

  String getMessage(JsonNode jsonNode);
}
