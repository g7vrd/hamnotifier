package uk.co.g7vrd.hamnotifier.feeds.wspr;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import uk.co.g7vrd.hamnotifier.Notifier;
import uk.co.g7vrd.hamnotifier.feeds.MessageGenerator;

import java.lang.reflect.Type;
import java.util.Collection;

@Component
public class WsprHandler implements StompFrameHandler {

  private final Collection<Notifier> notifiers;
  private final String callsign;
  private final MessageGenerator messageGenerator;

  @Autowired
  public WsprHandler(@Value("${callsign}") String callsign,
                     Collection<Notifier> notifiers,
                     WsprMessageGenerator wsprMessageGenerator) {
    this.messageGenerator = wsprMessageGenerator;
    Assert.notNull(callsign, "Callsign cannot be null. Specify with --callsign=XXX on the command line");
    Assert.isTrue(!callsign.isEmpty(), "Callsign must be specified. Specify with --callsign=XXX on the command line");
    this.notifiers = notifiers;
    this.callsign = callsign;
    System.out.println("WSPR handler started");
  }

  @Override
  public Type getPayloadType(StompHeaders headers) {
    return JsonNode.class;
  }

  @Override
  public void handleFrame(StompHeaders headers,
                          Object payload) {
    JsonNode wsprV1 = (JsonNode) payload;

    ArrayNode wsprs = (ArrayNode) wsprV1.at("/wsprs");

    for (JsonNode wspr : wsprs) {
      if (wspr.at("/remote").asText().equals(callsign)) {
        final String message = messageGenerator.getMessage(wspr);
        notifiers.forEach(notifier -> notifier.notify("WSPR", message));
      }
    }
  }
}
