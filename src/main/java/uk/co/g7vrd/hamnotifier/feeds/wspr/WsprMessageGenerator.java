package uk.co.g7vrd.hamnotifier.feeds.wspr;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Component;

@Component
public class WsprMessageGenerator implements uk.co.g7vrd.hamnotifier.feeds.MessageGenerator {

  @Override
  public String getMessage(JsonNode jsonNode) {
    final String rxCallsign = jsonNode.at("/source").asText();
    final String rxGrid = jsonNode.at("/sourcegrid").asText();
    final String band = jsonNode.at("/band").asText();
    final String khz = jsonNode.at("/khz").asText();
    final String km = jsonNode.at("/km").asText();
    final String report = jsonNode.at("/snr").asText();

    return report + " " + rxCallsign + "@" + rxGrid + " (" + km + "km) on " + khz + " " + band;
  }
}
