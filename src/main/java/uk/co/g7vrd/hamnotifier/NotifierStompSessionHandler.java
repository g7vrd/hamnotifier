package uk.co.g7vrd.hamnotifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.stereotype.Component;
import uk.co.g7vrd.hamnotifier.feeds.psk.PskHandler;
import uk.co.g7vrd.hamnotifier.feeds.skim.SkimHandler;
import uk.co.g7vrd.hamnotifier.feeds.spot.SpotHandler;
import uk.co.g7vrd.hamnotifier.feeds.wcy.WcyHandler;
import uk.co.g7vrd.hamnotifier.feeds.wspr.WsprHandler;

import java.lang.reflect.Type;

@Component
public class NotifierStompSessionHandler implements StompSessionHandler {

  private final PskHandler pskHandler;
  private final SkimHandler skimHandler;
  private final SpotHandler spotHandler;
  private final WcyHandler wcyHandler;
  private final WsprHandler wsprHandler;

  @Autowired
  public NotifierStompSessionHandler(PskHandler pskHandler,
                                     SkimHandler skimHandler,
                                     SpotHandler spotHandler,
                                     WcyHandler wcyHandler,
                                     WsprHandler wsprHandler) {
    this.pskHandler = pskHandler;
    this.skimHandler = skimHandler;
    this.spotHandler = spotHandler;
    this.wcyHandler = wcyHandler;
    this.wsprHandler = wsprHandler;
  }

  @Override
  public void afterConnected(StompSession stompSession,
                             StompHeaders stompHeaders) {
    System.out.println("Connected");
    stompSession.subscribe("/topic/psks/v1", pskHandler);
    stompSession.subscribe("/topic/skims/v1", skimHandler);
    stompSession.subscribe("/topic/spots/v1", spotHandler);
    stompSession.subscribe("/topic/wsprs/v1", wsprHandler);
    stompSession.subscribe("/topic/wcys/v1", wcyHandler);
  }

  @Override
  public void handleException(StompSession stompSession,
                              StompCommand stompCommand,
                              StompHeaders stompHeaders,
                              byte[] bytes,
                              Throwable throwable) {
    throwable.printStackTrace(System.err);
  }

  @Override
  public void handleTransportError(StompSession stompSession,
                                   Throwable throwable) {
    throwable.printStackTrace(System.err);
  }

  @Override
  public Type getPayloadType(StompHeaders stompHeaders) {
    System.err.println("getPayloadType");
    throw new RuntimeException();
  }

  @Override
  public void handleFrame(StompHeaders stompHeaders,
                          Object o) {
    System.err.println("handleFrame");
    throw new RuntimeException();
  }
}
