hamnotifier listens to event streams and alerts you when your callsign is heard 
somewhere.

It uses the STOMP websocket feeds from https://ws.g7vrd.co.uk/
It currently has:
 * Spots
 * Skims
 * PSK
 * WSPR
 * WCY

Please note: the PSK feeds are not all of the data sent to pskreporter. If you 
know where I can get this data to add to my feeds, please let me know.

Requirements:

Java JDK >= 8


Obtain:

git clone https://gitlab.com/g7vrd/hamnotifier


Build:

Linux: ./mvnw clean package
Windows: mvnw clean package


Run:

java -jar target/hamnotifier-0.0.1-SNAPSHOT.jar --callsign=XXXX